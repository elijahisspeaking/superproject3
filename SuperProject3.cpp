#include <iostream>


void example(bool even, int N)
{ 

    for (int i = even; i <= N; i += 2)
    {
        std::cout << i << "\n";
    }

}

int main()
{
    example(true, 15);
    example(false, 15);
}